# README #

On RHEL 7 or CentOS 7, install Cockpit by, 

```
# yum install epel-release
# yum install cockpit
```

Then checkout this repo in /usr/share/cockpit and restart (or start) the cockpit daemon, 

```
# git checkout <repo URL>
# systemctl restart cockpit
```

The nodejs based web server will start listening on TCP port 9090 

```
# ss -tunlp | grep cockpit
```